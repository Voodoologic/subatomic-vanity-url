Rails.application.routes.draw do
  resources :urls
  resources :users
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
  get '/:vanity', to: 'vanity_redirect#detour'
end
