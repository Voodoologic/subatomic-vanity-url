class Url < ApplicationRecord
  belongs_to :user
  validates :vanity, { exclusion: {in: [:urls, :users], message: "%{value} is reserved."}}
  validates :vanity, uniqueness: true
  validates :vanity, presence: true
  validates :vanity, format: { with: /\A[a-zA-Z0-9-]+\z/, message: "only allows letters, numbers and dashes" }
  validates :source, format: {
                with: %r(\A(http|https):\/\/|[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,6}(:[0-9]{1,5})?(\/.*)?$/ix\z),
                message: "only valid urls"
              }
end
