class UrlsController < ApplicationController
  before_action :set_url, only: %i[ show edit update destroy ]
  before_action :set_users, only: %i[ index new edit]
  before_action :set_user, only: %i[ show edit update destroy ]
  include HttpAuthConcern
  # GET /urls or /urls.json
  def index
    @urls = Url.all
  end

  # GET /urls/1 or /urls/1.json
  def show
    @user = User.find(@url.user_id)
  end

  # GET /urls/new
  def new
    @url = Url.new
  end

  # GET /urls/1/edit
  def edit
    if @active_user.nil? || @active_user != @user
      redirect_back(fallback_location: urls_path, alert: 'incorrect authentication')
    end
  end

  # POST /urls or /urls.json
  def create
    @url = Url.new(url_params)

    respond_to do |format|
      if @url.save
        format.html { redirect_to url_url(@url), notice: "Url was successfully created." }
        format.json { render :show, status: :created, location: @url }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @url.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /urls/1 or /urls/1.json
  def update
    respond_to do |format|
      if @url.update(url_params)
        format.html { redirect_to url_url(@url), notice: "Url was successfully updated." }
        format.json { render :show, status: :ok, location: @url }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @url.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /urls/1 or /urls/1.json
  def destroy
    if @active_user.nil? || @active_user != @user
      redirect_back(fallback_location: urls_path, alert: 'incorrect authentication') && return
    else
      @url.destroy
      redirect_to urls_url, notice: "Url was successfully destroyed."
    end
  end

  private
    def set_user
      @user = @url.user
    end
    def set_users
      @users = User.all
    end
    # Use callbacks to share common setup or constraints between actions.
    def set_url
      @url = Url.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def url_params
      params.require(:url).permit(:source, :vanity, :user_id)
    end
end
