class VanityRedirectController < ApplicationController
  def detour
    url = Url.find_by(vanity: vanity_param)
    if url
      url.increment(:visits).save
      redirect_to url.source, status: :found, allow_other_host: true
    else
      render file: "#{Rails.application.root}/public/404.html",  status: 404
    end
  end

  def vanity_param
    params.require(:vanity)
  end
end
