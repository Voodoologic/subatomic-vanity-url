json.extract! url, :id, :source, :vanity, :user_id, :created_at, :updated_at
json.url url_url(url, format: :json)
