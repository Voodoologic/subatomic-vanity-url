class CreateUrls < ActiveRecord::Migration[7.0]
  def change
    create_table :urls do |t|
      t.text :source
      t.string :vanity
      t.integer :user_id

      t.timestamps
    end
  end
end
